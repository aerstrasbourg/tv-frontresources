#! /bin/bash

# NGINX config
echo 'server {
    listen 80 default_server backlog=2048;

    root /var/www/www;
    index index.html index.php;

    # Make site accessible from http://localhost/
    server_name localhost;

    location / {
        try_files $uri $uri/ /index.php;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_index index.php;
' > /etc/nginx/sites-available/default

echo "	fastcgi_param FRONT_RESOURCES_DB_HOST '$FRONT_RESOURCES_DB_HOST';
	fastcgi_param FRONT_RESOURCES_DB_PORT '$FRONT_RESOURCES_DB_PORT';
	fastcgi_param FRONT_RESOURCES_DB_USERNAME '$FRONT_RESOURCES_DB_USERNAME';
	fastcgi_param FRONT_RESOURCES_DB_PASSWORD '$FRONT_RESOURCES_DB_PASSWORD';
	fastcgi_param FRONT_RESOURCES_DB_DATABASE '$FRONT_RESOURCES_DB_DATABASE';
" >> /etc/nginx/sites-available/default

echo '	include fastcgi_params;
    }

}' >> /etc/nginx/sites-available/default

# Mongo config
echo 'extension=mongo.so' > /etc/php5/cli/conf.d/50-mongo.ini
echo 'extension=mongo.so' > /etc/php5/fpm/conf.d/50-mongo.ini

# start php-fpm
service php5-fpm start

# start nginx
nginx
