FROM ubuntu

MAINTAINER Romain Vermot <romain.vermot@epitech.eu>

RUN apt-get update --fix-missing
RUN apt-get install -y nginx

RUN apt-get install -y curl
RUN apt-get install -y php5-fpm php5-cli php5-dev php-pear php5-curl php5-mongo php5-cgi

ENV FRONT_RESOURCES_DB_HOST=127.0.0.1
ENV FRONT_RESOURCES_DB_PORT=27017
ENV FRONT_RESOURCES_DB_USERNAME=
ENV FRONT_RESOURCES_DB_PASSWORD=

ENV FRONT_RESOURCES_DB_ROUTE=

EXPOSE 80

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD . /var/www

RUN cd /var/www && composer update

RUN chmod +x /var/www/startup.sh

CMD ["/bin/bash", "/var/www/startup.sh"]