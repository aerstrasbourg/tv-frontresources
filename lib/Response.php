<?php
/**
 * Response.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace lib;

class Response
{
    private static $_app = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function init(\Slim\Slim $app)
    {
        self::$_app = $app;
    }

    public static function json($data, $status = 200)
    {
        if (self::$_app === null)
            throw new \Exception('Response not initialized');

        self::$_app->contentType('application/json');
        self::$_app->response()->status($status);
        echo json_encode($data);
    }

    public static function raw($contentType, $raw)
    {
        if (self::$_app === null)
            throw new \Exception('Response not initialized');

        self::$_app->contentType($contentType);
        echo $raw;
    }
}