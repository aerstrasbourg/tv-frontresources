<?php
/**
 * DataBase.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace lib;

class DataBase
{
    private static $_instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            $host = getenv('FRONT_RESOURCES_DB_HOST');
            $port = getenv('FRONT_RESOURCES_DB_PORT');
            $username = getenv('FRONT_RESOURCES_DB_USERNAME');
            $password = getenv('FRONT_RESOURCES_DB_PASSWORD');
            $database = getenv('FRONT_RESOURCES_DB_DATABASE');

            try {
	      	if ($username && $password)
                    $m = new \Mongo("mongodb://$username:$password@$host:$port");
	        else
                    $m = new \Mongo("mongodb://$host:$port");
                self::$_instance = $m->$database;
            } catch (\MongoConnectionException $e) {
                Response::json('Error connecting to MongoDB server', 500);
                die();
            }
        }
        return self::$_instance;
    }
}
