<?php
/**
 * Router.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace lib;

use controllers;

class Router
{
    private static $_app = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function init(\Slim\Slim $app)
    {
        self::$_app = $app;
    }

    public static function call($method, $url, $action)
    {
        if (self::$_app === null)
            throw new \Exception('Router not initialized');

        return self::$_app->$method($url, function() use ($action) {
            $action = explode('@', $action);
            $controllerName = '\\app\\controllers\\' . $action[0] . 'Controller';
            $method = $action[1];
            $controller = new $controllerName(self::$_app);
            call_user_func_array([$controller, $method], func_get_args());
        });
    }

    public static function errors($method, $action)
    {
        if (self::$_app === null)
            throw new \Exception('Router not initialized');

        return self::$_app->$method(function() use ($action) {
            $action = explode('@', $action);
            $controllerName = '\\app\\controllers\\' . $action[0] . 'Controller';
            $method = $action[1];
            $controller = new $controllerName(self::$_app);
            call_user_func_array([$controller, $method], func_get_args());
        });
    }

    public static function get($url, $action)
    {
        return self::call('get', $url, $action);
    }

    public static function post($url, $action)
    {
        return self::call('post', $url, $action);
    }

    public static function put($url, $action)
    {
        return self::call('put', $url, $action);
    }

    public static function delete($url, $action)
    {
        return self::call('delete', $url, $action);
    }

    public static function patch($url, $action)
    {
        return self::call('patch', $url, $action);
    }

    public static function groupMiddleware($url, $function, $action)
    {
        if (self::$_app === null)
            throw new \Exception('Router not initialized');

        return self::$_app->group($url, $function, function() use ($action) {
            $action();
        });
    }

    public static function group($url, $action)
    {
        if (self::$_app === null)
            throw new \Exception('Router not initialized');

        return self::$_app->group($url, function() use ($action) {
            $action();
        });
    }

    public static function notFound($action)
    {
        if (self::$_app === null)
            throw new \Exception('Router not initialized');

        return self::$_app->notFound(function() use ($action) {
            $action = explode('@', $action);
            $controllerName = '\\app\\controllers\\' . $action[0] . 'Controller';
            $method = $action[1];
            $controller = new $controllerName(self::$_app);
            call_user_func_array([$controller, $method], func_get_args());
        });
    }
}