<?php
/**
 * routes.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace app;

use lib\Router;

Router::notFound('Errors@notFound');
Router::put('/assets', 'Assets@update');
Router::get('/assets/:id', 'Assets@show');
Router::delete('/assets/:id', 'Assets@destroy');
