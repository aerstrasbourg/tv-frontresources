<?php
/**
 * AssetsController.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace app\controllers;

use lib\DataBase;
use lib\Response;

class AssetsController extends BaseController
{
    private function storeFile(\MongoGridFS $gridFS, $id)
    {
        $url = getenv('FRONT_RESOURCES_ROUTE') . $id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $raw = substr($response, $headerSize);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close ($ch);

        if (!$raw)
            return false;

        $gridFS->storeBytes($raw, [
                '_id' => $id,
                'contentType' => $contentType
            ]
        );
        return true;
    }

    public function update()
    {
        $body = $this->_app->request->getBody();
        $idArray = json_decode($body);
        if (!is_array($idArray)) {
            return Response::json('invalid format of data received', 400);
        } else {
            $db = DataBase::getInstance();
            $gridFS = $db->getGridFS();

            foreach ($idArray as $id) {
                if ($id) {
                    $file = $gridFS->findOne(['_id' => $id]);
                    if (!$file) {
                        if (!$this->storeFile($gridFS, $id))
                            return Response::json('file not found', 400);
                    }
                } else {
                    return Response::json('invalid url format', 400);
                }
            }
            return Response::json('success', 200);
        }
    }

    public function show($id)
    {
        $db = DataBase::getInstance();
        $gridFS = $db->getGridFS();
        $file = $gridFS->findOne(['_id' => $id]);
        if ($file)
            return Response::raw($file->file['contentType'], $file->getBytes());
        else
            return Response::json('file not found', 400);
    }

    public function destroy($id)
    {
        $db = DataBase::getInstance();
        $gridFS = $db->getGridFS();
        $file = $gridFS->findOne(['_id' => $id]);
        if (!$file)
            return Response::json('file not found', 400);
        $id = $file->file['_id'];
        $gridFS->delete($id);
        return Response::json('file deleted', 200);
    }
}