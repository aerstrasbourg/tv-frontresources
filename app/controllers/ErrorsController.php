<?php
/**
 * ErrorsController.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace app\controllers;

use lib\Response;

class ErrorsController extends BaseController
{
    public function notFound()
    {
        Response::json('looks like this page does not exist', 404);
    }
}