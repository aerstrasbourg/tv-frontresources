<?php
/**
 * BaseController.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

namespace app\controllers;

class BaseController
{
    protected  $_app,
        $_sessionId;

    public function __construct(\Slim\Slim $app)
    {
        $this->_app = $app;
    }
}