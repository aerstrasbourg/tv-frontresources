<?php
/**
 * index.php
 *
 * @package    Resources
 * @author     Romain Vermot - vermot_r
 * @copyright  Copyright (c) 2015 Romain Vermot
 */

require '../vendor/autoload.php';

$app = new \Slim\Slim();
$app->config('debug', false);

\lib\Router::init($app);
\lib\Response::init($app);

require '../app/routes.php';

$app->run();
