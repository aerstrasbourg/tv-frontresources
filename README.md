# TV-FrontResources
## Description
This is the microservice used to manage assets in the frontoffice of the TV Project.

### Environments
```bash
MongoDB:
  FRONT_RESOURCES_DB_HOST
  FRONT_RESOURCES_DB_PORT
  FRONT_RESOURCES_DB_USERNAME
  FRONT_RESOURCES_DB_PASSWORD
  FRONT_RESOURCES_DB_DATABASE

Assets Route:
  FRONT_RESOURCES_ROUTE
```

## API
### Cached data
#### PUT on /assets
PUT on /assets a multipart request containing an array with the URL of the files you want to cache.
```json
[
  "id",
  "id",
]
```

### Get
#### GET on /assets/:id
GET on /assets/:id where id is the id of the file

This will response with the file content

### Deletion
#### DELETE on /assets/:id
DELETE on /assets/:id where id is the id of the file

```json
"file deleted"
```
